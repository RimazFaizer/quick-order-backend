/* eslint-disable import/no-anonymous-default-export */

export default {
  products: [
    {
    _id: '1',
    name: 'laptop',
    category: 'lap',
    image: '/images/websites.jpeg',
    price:'200',
    brand: 'dell',
    rating: 5.3,
    numReviews: 50
  },
  {
    _id: '2',
    name: 'Radio',
    category: 'telecome',
    image: '/images/radio3.webp',
    price:'200',
    brand: 'dell',
    rating: 3.1,
    numReviews: 22
  },
  {
    _id: '3',
    name: 'TV',
    category: 'telecome',
    image: '/images/tv.webp',
    price:'200',
    brand: 'dell',
    rating: 4.6,
    numReviews: 36
  },
]
}